package com.braisblanco.AmazonSQSClientApp.client;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SendMessageMain {
	
	private static final String echoInboxQueueUrl = "";
			
	private static final String echoOutboxQueueUrl = "";
	
	private static final String bucket = "";
	
	private static final String ECHO_MESSAGE_TYPE = "echo";
	
	private static final String SEARCH_MESSAGE_TYPE = "search";
	
	private static String nickname = "";
	
	private static String sessionId = "";
	
	public static void main(String args[]) throws Exception {
		
		signIn();
		
		while(true) {
			int option = mainMenu();
			
			switch(option) {
				case 1: //	CHAT
					chat();
					break;
				case 2: // SEARCH FOR MESSAGES
					searchForMessages();
					break;
				case 3:
					downloadPreviousConversation();
					break;
					
				case 4:
					System.exit(0);
					
				default: // DOWNLOAD PREVIOUS CHATS
					break;
			}
			
		}
	}
	
	private static void signIn() throws IOException {
		
		System.out.println("Welcome to the EchoSystem App!");
		System.out.println("Enter your username:");
		
		BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
		
		nickname = bufReader.readLine();
		
		sessionId = UUID.randomUUID().toString();
				
		System.out.println("You signed in as " + nickname + " and session id " + sessionId);
		
		System.out.println();
	}

	
	private static int mainMenu() throws IOException {
		
		BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("1) Chat.");
		System.out.println("2) Search for messages.");
		System.out.println("3) Download previous conversations.");
		System.out.println("4) Quit.");
		
		int option = Integer.parseInt(bufReader.readLine());
				
		return option;
	}
	
	private static void chat() throws IOException {
		
		
		AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
		
		SendMessageRequest sendMessageReq = new SendMessageRequest();
		sendMessageReq.setQueueUrl(echoInboxQueueUrl);
		
		MessageAttributeValue attribute = new MessageAttributeValue();
		attribute.setStringValue(nickname);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-id", attribute);
		
		attribute = new MessageAttributeValue();
		attribute.setStringValue(sessionId);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-sessionId", attribute);
		
		attribute = new MessageAttributeValue();
		attribute.setStringValue(ECHO_MESSAGE_TYPE);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("message-type", attribute);
		
		String messageBody = "";
		
		BufferedReader bufR = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			
			System.out.println("Type here the message:");
			
			messageBody = bufR.readLine();
			
			if(messageBody.equalsIgnoreCase("END")) {
				
				System.out.println();
				System.out.println("Leaving chat...");
				System.out.println();
				
				break;
			}
			
			System.out.println("Message sent: " + messageBody);
							
			sendMessageReq.setMessageBody(messageBody);
		
			sqs.sendMessage(sendMessageReq);
		
			while(true) {
			
				ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(echoOutboxQueueUrl).withMessageAttributeNames("All").withMaxNumberOfMessages(1).withWaitTimeSeconds(5);	
				ReceiveMessageResult result = sqs.receiveMessage(receiveMessageRequest);
			
				List<Message> messages = result.getMessages();
			
				if(messages.isEmpty()) {
					continue;
				}
			
				if(!messages.get(0).getMessageAttributes().get("client-id").getStringValue().equals(nickname)) {
					System.out.println("There are messages, none of them are for me.\n");
					continue;
				}
				
				if(!messages.get(0).getMessageAttributes().get("client-sessionId").getStringValue().equals(sessionId)) {
					System.out.println("There are messages, none of them are for me.\n");
					continue;
				}
			
				System.out.println("Message received: " + messages.get(0).getBody());	
				
				System.out.println();
				
				sqs.deleteMessage(new DeleteMessageRequest(echoOutboxQueueUrl, messages.get(0).getReceiptHandle()));
			
				break;
			}
		}
	}
	
	public static void downloadPreviousConversation() throws Exception {
		
		AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();
		ArrayList<String> previousChatsKeys = new ArrayList<>();
		ArrayList<ChatObject> chatsRetrieved = new ArrayList<ChatObject>();
		
		ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket).withPrefix(nickname.concat("/"));
		
		for (S3ObjectSummary s3ObSum : s3.listObjectsV2(req).getObjectSummaries()) {
			previousChatsKeys.add(s3ObSum.getKey());
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		for(String key: previousChatsKeys) {
		
			S3Object s3Obj = s3.getObject(bucket, key);
		  
			S3ObjectInputStream byteArray = s3Obj.getObjectContent();
		  
			chatsRetrieved.add(objectMapper.readValue(byteArray.readAllBytes(),ChatObject.class));

		}
		
		System.out.println("CHATS RETRIEVED FOR " + nickname);
				
		for(ChatObject chat : chatsRetrieved) {
			
			System.out.println("Session Id:" + chat.getSessionId());
			
			for(String message : chat.getChatMessages()) {
				System.out.println(message);
			}
		}
		
		System.out.println();
		
	}
	
	public static void searchForMessages() throws IOException {
		
		BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
		String textToFind = "";
		
		System.out.println();
		System.out.println("Type the text you want to search for: ");
		
		textToFind = bufReader.readLine();
		
		AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
		
		SendMessageRequest sendMessageReq = new SendMessageRequest();
		sendMessageReq.setQueueUrl(echoInboxQueueUrl);
		
		MessageAttributeValue attribute = new MessageAttributeValue();
		attribute.setStringValue(nickname);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-id", attribute);

		attribute = new MessageAttributeValue();
		attribute.setStringValue(sessionId);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-sessionId", attribute);
		
		attribute = new MessageAttributeValue();
		attribute.setStringValue(SEARCH_MESSAGE_TYPE);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("message-type", attribute);
		
		sendMessageReq.setMessageBody(textToFind);
		
		sqs.sendMessage(sendMessageReq);
		
		while(true) {
			
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(echoOutboxQueueUrl).withMessageAttributeNames("All").withMaxNumberOfMessages(1).withWaitTimeSeconds(5);	
			ReceiveMessageResult result = sqs.receiveMessage(receiveMessageRequest);
		
			List<Message> messages = result.getMessages();
		
			if(messages.isEmpty()) {
				continue;
			}
		
			if(!messages.get(0).getMessageAttributes().get("client-id").getStringValue().equals(nickname)) {
				System.out.println("There are messages, none of them are for me.\n");
				continue;
			}
			
			if(!messages.get(0).getMessageAttributes().get("client-sessionId").getStringValue().equals(sessionId)) {
				System.out.println("There are messages, none of them are for me.\n");
				continue;
			}
		
			System.out.println("Message received: " + messages.get(0).getBody());	
			
			System.out.println();
			
			sqs.deleteMessage(new DeleteMessageRequest(echoOutboxQueueUrl, messages.get(0).getReceiptHandle()));
		
			break;
			
		}
	}
}
